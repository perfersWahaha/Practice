package helloworld.example.com.practice.game;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by dell on 2017/11/8.
 */

public class MyCard extends FrameLayout {

    //卡片显示的值
    private int num = 0;
    //卡片显示值的控件
    private TextView label;

    public MyCard(@NonNull Context context) {
        super(context);
        initView();
    }

    //初始化布局
    private void initView(){
        label = new TextView(getContext());
        label.setBackgroundColor(0x33ffffff);
        label.setGravity(Gravity.CENTER);
        label.setTextSize(32);
        //设置label控件的大小
        LayoutParams p =new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        p.setMargins(10,10,0,0);
        addView(label,p);
    }

    public int getNum() {
        return num;
    }
    //设置num时 将num显示在Label（TextView）上
    public void setNum(int num) {
        this.num = num;
        if (num<=0) {
            label.setText("");
        }else{
            label.setText(num+"");
        }
    }
    //判断两张卡片的值是否相等
    public boolean equals(MyCard card) {
        return this.getNum() == card.getNum();
    }

}
