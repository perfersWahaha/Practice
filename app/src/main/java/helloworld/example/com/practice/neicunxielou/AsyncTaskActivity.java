package helloworld.example.com.practice.neicunxielou;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import helloworld.example.com.practice.R;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/25 0025.
 */

public class AsyncTaskActivity extends Activity {

    private MyTask myTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        myTask = new MyTask();
        myTask.execute("");
//        new AsyncTask<Void,Void,Void>(){
//            @Override
//            protected Void doInBackground(Void... voids) {
//                try {
//                    Thread.sleep(12000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//                super.onPostExecute(aVoid);
//            }
//        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private static class MyTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {

            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("i++的数字是：" + i);
            }
            for (int i = 0; i < 100; ++i) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("++i的数字是：" + i);
            }
            return null;
        }
    }

}
