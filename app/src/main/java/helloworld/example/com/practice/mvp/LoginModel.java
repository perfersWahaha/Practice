package helloworld.example.com.practice.mvp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Administrator on 2017/8/30 0030.
 */

public class LoginModel implements ILoginModel {

    private ILoginPersonter loginPersonter;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1001:
                    loginPersonter.loginSucess();
                    break;
                case 1002:
                    loginPersonter.loginStatus(001);
                    break;
            }
        }
    };

    public LoginModel(ILoginPersonter loginPersonter) {
        this.loginPersonter = loginPersonter;
    }

    @Override
    public void login(final String userName, final String passWord) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("wahaha", "准备登录");
                if (userName != null && passWord != null) {
                    if (userName.equals("666") && passWord.equals("123")) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.sendEmptyMessage(1001);
                    } else {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.sendEmptyMessage(1002);
                    }
                } else {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(1002);
                }
            }
        }).start();
    }
}
