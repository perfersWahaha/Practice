package helloworld.example.com.practice.mvpneicunxielou.view;

/**
 * Created by admin on 2017/3/15.
 */
public interface ILoginView {
    void showProgress();
    void hideProgress();
    String getName();
    String getPassword();
    void loginSuccess();
    void loginFailer();
}
