package helloworld.example.com.practice.game;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import helloworld.example.com.practice.R;

/**
 * Created by Administrator on 2017/11/10 0010.
 */

public class MyGameActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
    }
}
