package helloworld.example.com.practice.todoapp.tasks;

import android.support.annotation.NonNull;

import helloworld.example.com.practice.todoapp.data.Task;

/**
 * Created by Administrator on 2017/10/30 0030.
 */

public class TasksPresenter implements TasksContract.Presenter{

    private final TasksContract.View mTasksView;

    public TasksPresenter(TasksContract.View tasksView) {
        mTasksView = tasksView;
        mTasksView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void result(int requestCode, int resultCode) {

    }

    @Override
    public void loadTasks(boolean forceUpdate) {

    }

    @Override
    public void addNewTask() {

    }

    @Override
    public void openTaskDetails(@NonNull Task requestedTask) {

    }

    @Override
    public void completeTask(@NonNull Task completedTask) {

    }

    @Override
    public void activateTask(@NonNull Task activeTask) {

    }

    @Override
    public void clearCompletedTasks() {

    }

    @Override
    public void setFiltering(TasksFilterType requestType) {

    }

    @Override
    public TasksFilterType getFiltering() {
        return null;
    }
}
