package helloworld.example.com.practice.duoxiancheng;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.Nullable;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class HandlerThreadActivity extends Activity{

    private HandlerThread mHandlerThread;
    private Handler mHandler;
    private boolean tag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandlerThread = new HandlerThread("handler1");
        mHandlerThread.start();

        mHandler = new Handler(mHandlerThread.getLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                getData();
                if (tag){
                    mHandler.sendEmptyMessage(1002);
                }
            }
        };
    }

    private void getData(){
        try {//模拟耗时操作
            Thread.sleep((int)(Math.random()*4000+1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final int a = (int) (Math.random()*3000+1000);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LogUtils.e(a+"");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tag = true;
        mHandler.sendEmptyMessage(1001);
    }

    @Override
    protected void onPause() {
        super.onPause();
        tag = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeMessages(1002);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);

    }
}
