package helloworld.example.com.practice.neicunxielou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.squareup.leakcanary.RefWatcher;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.R;
import helloworld.example.com.practice.mvp.LoginActivity;

/**
 * Created by Administrator on 2017/9/25 0025.
 */

public class ThreadActivity extends Activity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThreadActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        /**
         * 之前内存泄露的代码
         */
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(60000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();


        new Thread(new MyRunnable()).start();
    }

    private static class MyRunnable implements Runnable{
        @Override
        public void run() {
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }
}
