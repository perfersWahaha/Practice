package helloworld.example.com.practice.retrofit;

/**
 * Created by Administrator on 2017/9/14 0014.
 */

public class ServiceResponse {

    private String AreaCode;
    private String SurveyUserID;
    private String RecordID;
    private String RelationID;
    private String OccurType;
    private String PestType;
    private String TaskID;
    private String TaskSatus;
    private String SubSatus;
    private String TaskSubSatus;
    private String Qualified;
    private String TrapperCode;
    private int SubID;
    private String nUserID;



    @Override
    public String toString() {
        return "ServiceResponse{" +
                "AreaCode='" + AreaCode + '\'' +
                ", SurveyUserID='" + SurveyUserID + '\'' +
                ", RecordID='" + RecordID + '\'' +
                ", RelationID='" + RelationID + '\'' +
                ", OccurType='" + OccurType + '\'' +
                ", PestType='" + PestType + '\'' +
                ", TaskID='" + TaskID + '\'' +
                ", TaskSatus='" + TaskSatus + '\'' +
                ", SubSatus='" + SubSatus + '\'' +
                ", TaskSubSatus='" + TaskSubSatus + '\'' +
                ", Qualified='" + Qualified + '\'' +
                ", TrapperCode='" + TrapperCode + '\'' +
                ", SubID=" + SubID +
                ", nUserID='" + nUserID + '\'' +
                '}';
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    public String getSurveyUserID() {
        return SurveyUserID;
    }

    public void setSurveyUserID(String surveyUserID) {
        SurveyUserID = surveyUserID;
    }

    public String getRecordID() {
        return RecordID;
    }

    public void setRecordID(String recordID) {
        RecordID = recordID;
    }

    public String getRelationID() {
        return RelationID;
    }

    public void setRelationID(String relationID) {
        RelationID = relationID;
    }

    public String getOccurType() {
        return OccurType;
    }

    public void setOccurType(String occurType) {
        OccurType = occurType;
    }

    public String getPestType() {
        return PestType;
    }

    public void setPestType(String pestType) {
        PestType = pestType;
    }

    public String getTaskID() {
        return TaskID;
    }

    public void setTaskID(String taskID) {
        TaskID = taskID;
    }

    public String getTaskSatus() {
        return TaskSatus;
    }

    public void setTaskSatus(String taskSatus) {
        TaskSatus = taskSatus;
    }

    public String getSubSatus() {
        return SubSatus;
    }

    public void setSubSatus(String subSatus) {
        SubSatus = subSatus;
    }

    public String getTaskSubSatus() {
        return TaskSubSatus;
    }

    public void setTaskSubSatus(String taskSubSatus) {
        TaskSubSatus = taskSubSatus;
    }

    public String getQualified() {
        return Qualified;
    }

    public void setQualified(String qualified) {
        Qualified = qualified;
    }

    public String getTrapperCode() {
        return TrapperCode;
    }

    public void setTrapperCode(String trapperCode) {
        TrapperCode = trapperCode;
    }

    public int getSubID() {
        return SubID;
    }

    public void setSubID(int subID) {
        SubID = subID;
    }

    public String getnUserID() {
        return nUserID;
    }

    public void setnUserID(String nUserID) {
        this.nUserID = nUserID;
    }
}
