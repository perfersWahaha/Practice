package helloworld.example.com.practice.duoxiancheng;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class MyIntentService extends IntentService {

    private static final String ACTION_UPLOAD_IMG = "com.zhy.blogcodes.intentservice.action.UPLOAD_IMAGE";
    public static final String EXTRA_IMG_PATH = "com.zhy.blogcodes.intentservice.extra.IMG_PATH";

    public static void startGetData(Context context, String path) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_UPLOAD_IMG);
        intent.putExtra(EXTRA_IMG_PATH, path);
        context.startService(intent);
    }

    public MyIntentService() {
        super("MyIntentServicer");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent!=null){
            String action = intent.getAction();
            if (ACTION_UPLOAD_IMG.equals(action)){
                getData(intent.getStringExtra(EXTRA_IMG_PATH));
            }
        }
    }

    private void getData(String path) {
        try {
            Thread.sleep(3000);
            Intent intent = new Intent(IntentServiceActivity.UPLOAD_RESULT);
            intent.putExtra(EXTRA_IMG_PATH, path);
            sendBroadcast(intent);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
