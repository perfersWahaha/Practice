package helloworld.example.com.practice.todoapp.tasks;

/**
 * Created by Administrator on 2017/10/30 0030.
 */

public enum  TasksFilterType {
    /**
     * Do not filter tasks.
     */
    ALL_TASKS,

    /**
     * Filters only the active (not completed yet) tasks.
     */
    ACTIVE_TASKS,

    /**
     * Filters only the completed tasks.
     */
    COMPLETED_TASKS
}
