package helloworld.example.com.practice.recycleview;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import helloworld.example.com.practice.R;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/10/20 0020.
 */

public class RecyclerViewAdapter extends BaseQuickAdapter<ProjectBean,BaseViewHolder>{

    public RecyclerViewAdapter(@LayoutRes int layoutResId, @Nullable List<ProjectBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProjectBean item) {
        helper.setText(R.id.project_name,item.getName());
    }
}
