package helloworld.example.com.practice.mvp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.leakcanary.RefWatcher;

import java.util.logging.Logger;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.R;

/**
 * Created by Administrator on 2017/8/30 0030.
 */

public class LoginActivity extends AppCompatActivity implements ILoginView {

    private ILoginPersonter loginPersonter;
    private EditText userName;
    private EditText passWord;
    private TextView login;
    private ProgressDialog p;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPersonter = new LoginPersenter(this);
        initView();
        setListener();
    }

    private void initView() {
        userName = (EditText) findViewById(R.id.user_name);
        passWord = (EditText) findViewById(R.id.pass_word);
        login = (TextView) findViewById(R.id.login);
    }

    private void setListener() {
        login.setOnClickListener(onClickListener);
    }

    @Override
    public String getUserName() {
        return userName.getText().toString();
    }

    @Override
    public String getPassWord() {
        return passWord.getText().toString();
    }

    @Override
    public void dimessProgress() {
        p.dismiss();
    }

    @Override
    public void showProgress() {
        p = ProgressDialog.show(this, "提示", "正在加载...");
    }

    @Override
    public void showView() {
        Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFailedError() {
        Toast.makeText(this, "登录失败", Toast.LENGTH_SHORT).show();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.login) {
                Log.e("wahaha", "点击了登录");
                loginPersonter.loginToSercer();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
