package helloworld.example.com.practice.mvp;

/**
 * Created by Administrator on 2017/8/30 0030.
 */

public class LoginPersenter implements ILoginPersonter{

    private ILoginModel loginModel;
    private ILoginView loginActivity;


    public LoginPersenter(ILoginView loginActivity) {
        this.loginActivity = loginActivity;
        this.loginModel = new LoginModel(this);
    }

    @Override
    public void loginToSercer() {
        loginActivity.showProgress();
        loginModel.login(loginActivity.getUserName(),loginActivity.getPassWord());
    }

    @Override
    public void loginSucess() {
        loginActivity.dimessProgress();
        loginActivity.showView();
    }

    @Override
    public void loginStatus(int a) {
        if (a==001){
            loginActivity.dimessProgress();
            loginActivity.showFailedError();
        }
    }
}
