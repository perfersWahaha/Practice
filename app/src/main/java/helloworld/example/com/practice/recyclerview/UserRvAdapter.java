package helloworld.example.com.practice.recyclerview;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.NoSuchElementException;

import helloworld.example.com.practice.R;

/**
 * Created by dell on 2017/11/7.
 */

public class UserRvAdapter extends RecyclerView.Adapter<UserRvAdapter.UserViewHolder> {

    private Context context;
    private List<MainActivity.User> list;

    //控制是否显示删除
    private boolean isShowDelete;
    //删除按钮的点击监听
    private OnDeleteClickListener deleteClickListener;

    public UserRvAdapter(Context context, List<MainActivity.User> list) {
        this.context = context;
        this.list = list;
        //默认不显示删除
        isShowDelete = false;
    }

    public void setShowDelete(boolean showDelete) {
        isShowDelete = showDelete;
    }

    public boolean isShowDelete() {
        return isShowDelete;
    }

    @Override//获取Holder对象（获取item的布局文件及item控件封装到holder对象中）
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserViewHolder holder = new UserViewHolder(LayoutInflater.from(context).inflate(R.layout.recycleview_item, parent, false));
        return holder;
    }

    @Override//绑定数据
    public void onBindViewHolder(final UserViewHolder holder, int position) {

        holder.userNameSex.setText(list.get(position).getName() + "(" + list.get(position).getSex() + ")");
        holder.userAge.setText(list.get(position).getAge() + "岁");
        //判断是否显示删除和头像
        if (isShowDelete) {
            startOpenAnimator(holder);
        } else {
            stopOpenAnimator(holder);
        }

        //设置删除按钮点击事件
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deleteClickListener != null) {
                    deleteClickListener.onDeleteClick(v, holder.getLayoutPosition(), list.get(holder.getLayoutPosition()));
                    return;
                }
                throw new NoSuchElementException("删除按钮点击时，没有设置该按钮点回调方法！请用UserRvAdapter的实例对象调用setOnDeleteClickListener()方法！！！");
            }
        });
    }

    @Override//item条目（item的数量）
    public int getItemCount() {
        return list.size();
    }

    //ViewHolder类
    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView userAge;
        TextView userNameSex;
        TextView tvDelete;
        ImageView userHead;
        LinearLayout userContent;

        public UserViewHolder(View itemView) {
            super(itemView);
            userAge = (TextView) itemView.findViewById(R.id.user_age);
            userNameSex = (TextView) itemView.findViewById(R.id.user_name_sex);
            tvDelete = (TextView) itemView.findViewById(R.id.tv_delete);
            userHead = (ImageView) itemView.findViewById(R.id.user_head);
            userContent = (LinearLayout) itemView.findViewById(R.id.user_content);
        }
    }

    //删除按钮的点击监听
    public interface OnDeleteClickListener {
        void onDeleteClick(View v, int position, MainActivity.User user);
    }

    //删除按钮的点击监听回调
    public void setOnDeleteClickListener(OnDeleteClickListener listener) {
        this.deleteClickListener = listener;
    }

    private void startOpenAnimator(UserViewHolder holder) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(holder.tvDelete, "translationX", -200f, 0);
        anim1.setDuration(600);
        anim1.start();
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(holder.userHead, "translationX", 0, 200f);
        anim2.setDuration(600);
        anim2.start();
    }

    private void stopOpenAnimator(UserViewHolder holder) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(holder.tvDelete, "translationX",0, -200);
        anim1.setDuration(600);
        anim1.start();
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(holder.userHead, "translationX", 200f, 0);
        anim2.setDuration(600);
        anim2.start();
    }

}
