package helloworld.example.com.practice.retrofit;

/**
 * Created by Administrator on 2017/8/4 0004.
 */

public class QsmsInvolvingwoodenterprisesBean {

    /**
     * CountyName :
     * TownName :
     * VillageName :
     * AreaName :
     * RealName : null
     * IWETypeName : 苗木、生产、经营
     * CompanyTypeName : 民营企业
     * ID : 69503379-f19a-4135-8734-6ab6a4dab741
     * CompanyName : 重庆瑞升科技有限公司
     * Principal : 张晓红
     * LegalCode : null
     * CardNO : null
     * RegisteredAddress : 九龙坡区渝州路1126号附8-11-1
     * ContractPhone : 13068308178
     * BusinessAddress : 九龙坡区渝州路1126号附8-11-1
     * IWEType : 2,3,4
     * CompanyType : 5
     * OrganizationCode : null
     * BusinesLicenseCode : null
     * TaxCode : null
     * QuarantineCertificateID : null
     * AreaCode : 00107012001
     * RegistDate : 42324
     * RegistMoney : null
     * Longitude : null
     * Latitude : null
     * IsDel : null
     * FillFormTime : /Date(1495814400000)/
     * UserID : null
     * Description : null
     * BusinessType : 花卉苗木
     * PAPType : 花卉苗木
     * JYRY : null
     * BJYDH : null
     * EntityState : 1
     * EntityKey : null
     */

    private String CountyName;
    private String TownName;
    private String VillageName;
    private String AreaName;
    private String RealName;
    private String IWETypeName;
    private String CompanyTypeName;
    private String ID;
    private String CompanyName;
    private String Principal;
    private String LegalCode;
    private String CardNO;
    private String RegisteredAddress;
    private String ContractPhone;
    private String BusinessAddress;
    private String IWEType;
    private String CompanyType;
    private String OrganizationCode;
    private String BusinesLicenseCode;
    private String TaxCode;
    private String QuarantineCertificateID;
    private String AreaCode;
    private String RegistDate;
    private String RegistMoney;
    private String Longitude;
    private String Latitude;
    private String IsDel;
    private String FillFormTime;
    private String UserID;
    private String Description;
    private String BusinessType;
    private String PAPType;
    private String JYRY;
    private String BJYDH;
    private int EntityState;
    private String EntityKey;

    @Override
    public String toString() {
        return "QsmsInvolvingwoodenterprisesBean{" +
                "CountyName='" + CountyName + '\'' +
                ", TownName='" + TownName + '\'' +
                ", VillageName='" + VillageName + '\'' +
                ", AreaName='" + AreaName + '\'' +
                ", RealName='" + RealName + '\'' +
                ", IWETypeName='" + IWETypeName + '\'' +
                ", CompanyTypeName='" + CompanyTypeName + '\'' +
                ", ID='" + ID + '\'' +
                ", CompanyName='" + CompanyName + '\'' +
                ", Principal='" + Principal + '\'' +
                ", LegalCode='" + LegalCode + '\'' +
                ", CardNO='" + CardNO + '\'' +
                ", RegisteredAddress='" + RegisteredAddress + '\'' +
                ", ContractPhone='" + ContractPhone + '\'' +
                ", BusinessAddress='" + BusinessAddress + '\'' +
                ", IWEType='" + IWEType + '\'' +
                ", CompanyType='" + CompanyType + '\'' +
                ", OrganizationCode='" + OrganizationCode + '\'' +
                ", BusinesLicenseCode='" + BusinesLicenseCode + '\'' +
                ", TaxCode='" + TaxCode + '\'' +
                ", QuarantineCertificateID='" + QuarantineCertificateID + '\'' +
                ", AreaCode='" + AreaCode + '\'' +
                ", RegistDate='" + RegistDate + '\'' +
                ", RegistMoney='" + RegistMoney + '\'' +
                ", Longitude='" + Longitude + '\'' +
                ", Latitude='" + Latitude + '\'' +
                ", IsDel='" + IsDel + '\'' +
                ", FillFormTime='" + FillFormTime + '\'' +
                ", UserID='" + UserID + '\'' +
                ", Description='" + Description + '\'' +
                ", BusinessType='" + BusinessType + '\'' +
                ", PAPType='" + PAPType + '\'' +
                ", JYRY='" + JYRY + '\'' +
                ", BJYDH='" + BJYDH + '\'' +
                ", EntityState=" + EntityState +
                ", EntityKey='" + EntityKey + '\'' +
                '}';
    }

    public String getCountyName() {
        return CountyName;
    }

    public void setCountyName(String countyName) {
        CountyName = countyName;
    }

    public String getTownName() {
        return TownName;
    }

    public void setTownName(String townName) {
        TownName = townName;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        VillageName = villageName;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public String getRealName() {
        return RealName;
    }

    public void setRealName(String realName) {
        RealName = realName;
    }

    public String getIWETypeName() {
        return IWETypeName;
    }

    public void setIWETypeName(String IWETypeName) {
        this.IWETypeName = IWETypeName;
    }

    public String getCompanyTypeName() {
        return CompanyTypeName;
    }

    public void setCompanyTypeName(String companyTypeName) {
        CompanyTypeName = companyTypeName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getPrincipal() {
        return Principal;
    }

    public void setPrincipal(String principal) {
        Principal = principal;
    }

    public String getLegalCode() {
        return LegalCode;
    }

    public void setLegalCode(String legalCode) {
        LegalCode = legalCode;
    }

    public String getCardNO() {
        return CardNO;
    }

    public void setCardNO(String cardNO) {
        CardNO = cardNO;
    }

    public String getRegisteredAddress() {
        return RegisteredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        RegisteredAddress = registeredAddress;
    }

    public String getContractPhone() {
        return ContractPhone;
    }

    public void setContractPhone(String contractPhone) {
        ContractPhone = contractPhone;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        BusinessAddress = businessAddress;
    }

    public String getIWEType() {
        return IWEType;
    }

    public void setIWEType(String IWEType) {
        this.IWEType = IWEType;
    }

    public String getCompanyType() {
        return CompanyType;
    }

    public void setCompanyType(String companyType) {
        CompanyType = companyType;
    }

    public String getOrganizationCode() {
        return OrganizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        OrganizationCode = organizationCode;
    }

    public String getBusinesLicenseCode() {
        return BusinesLicenseCode;
    }

    public void setBusinesLicenseCode(String businesLicenseCode) {
        BusinesLicenseCode = businesLicenseCode;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(String taxCode) {
        TaxCode = taxCode;
    }

    public String getQuarantineCertificateID() {
        return QuarantineCertificateID;
    }

    public void setQuarantineCertificateID(String quarantineCertificateID) {
        QuarantineCertificateID = quarantineCertificateID;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    public String getRegistDate() {
        return RegistDate;
    }

    public void setRegistDate(String registDate) {
        RegistDate = registDate;
    }

    public String getRegistMoney() {
        return RegistMoney;
    }

    public void setRegistMoney(String registMoney) {
        RegistMoney = registMoney;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getIsDel() {
        return IsDel;
    }

    public void setIsDel(String isDel) {
        IsDel = isDel;
    }

    public String getFillFormTime() {
        return FillFormTime;
    }

    public void setFillFormTime(String fillFormTime) {
        FillFormTime = fillFormTime;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getPAPType() {
        return PAPType;
    }

    public void setPAPType(String PAPType) {
        this.PAPType = PAPType;
    }

    public String getJYRY() {
        return JYRY;
    }

    public void setJYRY(String JYRY) {
        this.JYRY = JYRY;
    }

    public String getBJYDH() {
        return BJYDH;
    }

    public void setBJYDH(String BJYDH) {
        this.BJYDH = BJYDH;
    }

    public int getEntityState() {
        return EntityState;
    }

    public void setEntityState(int entityState) {
        EntityState = entityState;
    }

    public String getEntityKey() {
        return EntityKey;
    }

    public void setEntityKey(String entityKey) {
        EntityKey = entityKey;
    }
}
