package helloworld.example.com.practice.mvpneicunxielou.presenter;

import android.os.Handler;

import helloworld.example.com.practice.mvpneicunxielou.base.BasePresenter;
import helloworld.example.com.practice.mvpneicunxielou.biz.ILoginBiz;
import helloworld.example.com.practice.mvpneicunxielou.biz.ILoginLisener;
import helloworld.example.com.practice.mvpneicunxielou.biz.LoginBizImpl;
import helloworld.example.com.practice.mvpneicunxielou.view.ILoginView;

/**
 * Created by admin on 2017/3/15.
 */
public class LoginPresenter extends BasePresenter {
    ILoginBiz iLoginBiz;
    ILoginView iLoginView;
    public LoginPresenter(ILoginView iLoginView) {
        this.iLoginView=iLoginView;
        iLoginBiz=new LoginBizImpl();//需构造的是实现类
    }

    Handler mHandler = new Handler();
    public void login() {
        iLoginView.showProgress();
        //执行耗时操作
        iLoginBiz.login(iLoginView.getName(), iLoginView.getPassword(), new ILoginLisener() {
            @Override
            public void loginSuccess() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        iLoginView.hideProgress();//然后toast提示成功
                        iLoginView.loginSuccess();
                    }
                });

            }

            @Override
            public void loginFaile() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        iLoginView.hideProgress();//然后toast提示失败
                        iLoginView.loginFailer();
                    }
                });

            }
        });
    }
}
