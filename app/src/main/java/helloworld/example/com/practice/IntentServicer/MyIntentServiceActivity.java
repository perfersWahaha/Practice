package helloworld.example.com.practice.IntentServicer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.leakcanary.RefWatcher;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.R;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class MyIntentServiceActivity extends Activity {

    public static final String ACTION_ONE = "open";
    public static final String ACTION_TWO = "close";
    BroadcastReceiver broadcastReceiver = new MyBroadcastReceiver();

    private TextView serviceStatus;
    private TextView threadStatus;
    private ProgressBar mProgressBar;
    private TextView mCount;
    private Button openService;
    private Button closeService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.resource.layout.activity_intent_service);
        setContentView(R.layout.activity_intent_service);
        registerReciver();
        initView();
        setListner();
    }

    private void initView() {
        serviceStatus = findViewById(R.id.service_status);
        threadStatus = findViewById(R.id.thread_status);
        mProgressBar = findViewById(R.id.progress);
        mCount = findViewById(R.id.count);
        openService = findViewById(R.id.open);
        closeService = findViewById(R.id.close);
    }

    private void setListner() {
        openService.setOnClickListener(onClickListener);
        closeService.setOnClickListener(onClickListener);
    }

    private void registerReciver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_ONE);
        intentFilter.addAction(ACTION_TWO);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case ACTION_ONE:
                    serviceStatus.setText(intent.getStringExtra("status"));
                    break;
                case ACTION_TWO:
                    int progress = intent.getIntExtra("progress", 0);
                    mProgressBar.setProgress(progress);
                    threadStatus.setText(intent.getStringExtra("status"));
                    mCount.setText(progress + "%");
                    break;
            }
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.open:
                    Intent intent = new Intent(MyIntentServiceActivity.this, MyIntentServicer.class);
                    MyIntentServiceActivity.this.startService(intent);
                    break;
                case R.id.close:
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }
}
