package helloworld.example.com.practice.retrofit;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/9/15 0015.
 */

public class MyRequestParameters {

    private Map<String, String> requestMap = new HashMap<>();

    public Map<String, String> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, String> requestMap) {
        this.requestMap = requestMap;
    }

    public void put(String key, String value) {
        requestMap.put(key, value);
    }

    public String buildRequestString() {
        //entrySet返回Map集合里的所有键值对
        Set<Map.Entry<String, String>> entries = requestMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        StringBuffer stringBuffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            stringBuffer.append("\""+entry.getKey() + "\""+":" +"\""+ entry.getValue() +"\"" +",");
        }
        return "{"+stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1)+"}";
    }

    public String buildRequestJson(){
        Gson gson = new Gson();
        return gson.toJson(requestMap);
    }
}
