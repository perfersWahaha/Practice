package helloworld.example.com.practice.neicunxielou;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.squareup.leakcanary.RefWatcher;

import java.lang.ref.WeakReference;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.R;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/25 0025.
 */

public class HandlerActivity extends Activity {

    /**
     * 会内存泄露的代码
     *
     * @param savedInstanceState
     */
//    private Handler handler = new Handler(){
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            Log.e("wahaha","我是憨豆");
//        }
//    };

    private Handler mUIThread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        mUIThread = new MyHandler(this);
        final MyRunnable myRunnable = new MyRunnable();
        Thread t = new Thread(myRunnable);
        t.start();
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRunnable.isRun = false;
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }

    private static class MyHandler extends Handler {

        private final WeakReference<HandlerActivity> mActivity;

        public MyHandler(HandlerActivity activity){
            mActivity = new WeakReference<HandlerActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mActivity.get() != null) {
                LogUtils.e("我是憨豆");
            }
        }
    }

    private static class MyRunnable implements Runnable {
        //标志位退出子线程
        private volatile boolean isRun = true;

        @Override
        public void run() {
            while (isRun) {
                LogUtils.e("数字1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字3");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字4");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字5");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字6");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字7");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字8");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("数字9");
            }
        }
    }
}
