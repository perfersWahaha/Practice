package helloworld.example.com.practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import helloworld.example.com.practice.translucentstatubar.ChangeColorStatuBarActivity;
import helloworld.example.com.practice.translucentstatubar.TranslucentsStatuBarActivity;

public class MainActivity extends AppCompatActivity {

    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setListener();
    }

    private void initView(){
        button1 = (Button) findViewById(R.id.button1);
        button1.setText("透明状态栏");
        button2= (Button) findViewById(R.id.button2);
        button2.setText("图片透明状态栏");
    }

    private void setListener(){
        button1.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.button1:
                    setIntent(helloworld.example.com.practice.mvpneicunxielou.MainActivity.class);
                    break;
                case R.id.button2:
                    setIntent(ChangeColorStatuBarActivity.class);
                    break;
            }
        }
    };

    private void setIntent(Class cls){
        Intent intent = new Intent(this,cls);
        startActivity(intent);
    }
}
