package helloworld.example.com.practice.rxjava;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.squareup.leakcanary.RefWatcher;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.components.RxActivity;

import helloworld.example.com.practice.ExampleApplication;
import helloworld.example.com.practice.utils.LogUtils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Administrator on 2017/10/11 0011.
 */

public class RxJavaTestActivity extends RxActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test6();
//        test7();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }

    private void test1() {
        /**
         * 简单的rxjava使用
         */

        //创建一个被观察者，发送数据
        Observable<Integer> observable = Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                e.onNext(1);
                e.onNext(2);
                e.onNext(3);
                e.onComplete();
            }
        });

        //观察者
        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                LogUtils.e("onSubscribe");
            }

            @Override
            public void onNext(@NonNull Integer integer) {
                LogUtils.e("onNext" + integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                LogUtils.e("onError");
            }

            @Override
            public void onComplete() {
                LogUtils.e("onComplete");
            }
        };
        //通过subscribe将他们联系起来
        observable.subscribe(observer);
    }

    private void test2() {
        /**
         * 连接起来使用rxjava
         * ObservableEmitter 就是一个发射器，用来发出事件，可以发出onNext(),onComplete(),onError()
         * 上游发送onComplete后下游在收到后不在继续接收事件
         * 上游发送onError后下游在收到后不在继续接收事件
         * onComplete和onError只能存在一个，且只能发送一个
         */
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                e.onNext(1001);
                e.onNext(1002);
                e.onNext(1003);
                e.onComplete();
            }
        }).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                LogUtils.e("onSubscribe");
            }

            @Override
            public void onNext(@NonNull Integer integer) {
                LogUtils.e("onNext" + integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                LogUtils.e("onError");
            }

            @Override
            public void onComplete() {
                LogUtils.e("onComplete");
            }
        });
    }

    private void test3() {
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                LogUtils.e("发送线程是：" + Thread.currentThread().getName());
                LogUtils.e("发送了:1");
                e.onNext(1);

                LogUtils.e("发送了:2");
                e.onNext(2);

                LogUtils.e("发送了:3");
                e.onNext(3);

                LogUtils.e("发送了:onComplete");
                e.onComplete();

                LogUtils.e("发送了:4");
                e.onNext(4);
            }
        }).subscribe(new Observer<Integer>() {
            //Disposable用来切断连接
            private Disposable disposable;
            private int i;

            @Override
            public void onSubscribe(@NonNull Disposable d) {
                LogUtils.e("onSubscribe");
            }

            @Override
            public void onNext(@NonNull Integer integer) {
                LogUtils.e("接收线程是：" + Thread.currentThread().getName());
                LogUtils.e("onNext:" + integer);
                i++;
                if (i == 2) {
                    LogUtils.e("dispose");
                    disposable.dispose();
                    LogUtils.e("是否切断:" + disposable.isDisposed());
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                LogUtils.e("onError");
            }

            @Override
            public void onComplete() {
                LogUtils.e("onComplete");
            }
        });
    }

    //Scheduler 调度器来改变所在的线程
    private void test4() {
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                LogUtils.e("发送线程是：" + Thread.currentThread().getName());
                e.onNext(1);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Integer integer) {
                LogUtils.e("接收线程是：" + Thread.currentThread().getName());
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void test5() {
        Observable.create(new ObservableOnSubscribe() {
            @Override
            public void subscribe(@NonNull ObservableEmitter e) throws Exception {
                e.onNext("你好");
            }
        }).subscribe(new Observer() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Object o) {
                LogUtils.e(o.toString());
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    //打印字符串数组
    private void test6() {
        String[] s = new String[]{"张三", "李四", "王麻子"};
        Observable.fromArray(s).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                LogUtils.e("String的值:" + s);
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //线程切换 subscribeOn指定当前线程observeOn指定之后线程
    private void test7() {
        Observable
                .create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                        Thread.sleep(3000);
                        for (int i = 0; i <Integer.MAX_VALUE ; i++) {
                            Thread.sleep(500);
                            LogUtils.e("变身信号:" + i);
                            e.onNext(i);
                        }
                        e.onComplete();
                    }
                })
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        LogUtils.e("我要变身了");
                        Toast.makeText(getApplicationContext(), "变身", Toast.LENGTH_SHORT).show();
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<Integer>bindToLifecycle())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Object o) {
                        LogUtils.e("收到:" + o);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
