package helloworld.example.com.practice.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import helloworld.example.com.practice.utils.LogUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2017/9/14 0014.
 */

public class ApiController {

    private static Gson sGson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:SS")
            .serializeNulls()
            .create();

    private static Api getApi() {
        return ApiSingleton.mInstance;
    }

    private static class ApiSingleton {
        private static Api mInstance = ApiHelper.retrofit(BaseUrl.BASE_URL).create(Api.class);
    }

    public static void getDataBaseData(String type, String TaskID, String TrapperCode, final RetrofitListener<List<?>> callback) {

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setTaskID(TaskID);
        serviceResponse.setTrapperCode(TrapperCode);
        String jsonResponse = sGson.toJson(serviceResponse);

        getApi().getDataBaseData(type, jsonResponse).enqueue(new Callback<List<ForestHostModel>>() {
            @Override
            public void onResponse(Call<List<ForestHostModel>> call, Response<List<ForestHostModel>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<ForestHostModel>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public static void getTData(String type, String TaskID, String TrapperCode, final Class clazz, final RetrofitListener<List<?>> callback) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setTaskID(TaskID);
        serviceResponse.setTrapperCode(TrapperCode);
        String jsonResponse = sGson.toJson(serviceResponse);
        getApi().getTData(type, jsonResponse).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        ArrayList list = JsonDataFactory.jsonToArrayList(response.body().string(), clazz);
                        callback.onSuccess(list);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                LogUtils.e("error =" + t.getMessage());
            }
        });
    }



    public static void getData(String type,String json, final Class clazz, final RetrofitListener<List<?>> callback) {

        getApi().getTData(type,json).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        ArrayList list = JsonDataFactory.jsonToArrayList(response.body().string(), clazz);
                        callback.onSuccess(list);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
}
