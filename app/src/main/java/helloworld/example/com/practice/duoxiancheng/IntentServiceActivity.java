package helloworld.example.com.practice.duoxiancheng;

import android.app.Activity;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;

import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class IntentServiceActivity extends Activity{

    public static final String UPLOAD_RESULT = "com.zhy.blogcodes.intentservice.UPLOAD_RESULT";

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction()==UPLOAD_RESULT){
                String path = intent.getStringExtra(MyIntentService.EXTRA_IMG_PATH);
                getResult(path);
            }
        }
    };

    private void getResult(String path){
        LogUtils.e("path = "+path);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        register();


        start();
    }

    private void register(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UPLOAD_RESULT);
        registerReceiver(receiver,intentFilter);
    }

    private void start(){
        int j = 0;
        for (int i = 0; i <1000 ; i++) {
            //模拟路径
            String path = "/sdcard/imgs/" + i+ ".png";
            MyIntentService.startGetData(this, path);
        }

    }
}
