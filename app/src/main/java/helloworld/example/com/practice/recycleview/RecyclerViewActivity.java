package helloworld.example.com.practice.recycleview;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.ArrayList;
import java.util.List;

import helloworld.example.com.practice.R;
import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/10/20 0020.
 */

public class RecyclerViewActivity extends Activity{

    List<ProjectBean> list;
    private RecyclerView recyclerView;
    private EditText edittext;
    RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        recyclerView = findViewById(R.id.recycler_view);
        edittext = findViewById(R.id.edit_text);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<>();
//        for (int i = 0; i <30 ; i++) {
//             p= new ProjectBean();
//            p.setName("金山"+i);
//            list.add(p);
//        }
        getData();
        adapter= new RecyclerViewAdapter(R.layout.item_recycler_view,list);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                LogUtils.e("你点击了"+list.get(position).getName());
            }
        });

        edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LogUtils.e("s:"+s.toString());
                changeList(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeList(String s){
        List<ProjectBean> bean = new ArrayList<>();
        if (TextUtils.isEmpty(s)){
            LogUtils.e(list+"");
            bean.addAll(getData());
        }else {
            bean.clear();
            for (int i = 0; i <list.size() ; i++) {
                String name = list.get(i).getName();
                if (name.indexOf(s.toString())!=-1){
                    bean.add(list.get(i));
                }
            }
        }
        list.clear();
        list.addAll(bean);
        adapter.notifyDataSetChanged();
    }

    private List<ProjectBean> getData(){
        ProjectBean p;
        p = new ProjectBean();
        p.setName("金山沙发斯蒂芬");
        list.add(p);
        p = new ProjectBean();
        p.setName("士大夫还是发");
        list.add(p);
        p = new ProjectBean();
        p.setName("士大夫三发电公司大");
        list.add(p);
        p = new ProjectBean();
        p.setName("发生的发生的高手");
        list.add(p);
        p = new ProjectBean();
        p.setName("第三方撒尴尬");
        list.add(p);
        p = new ProjectBean();
        p.setName("三大法师打发");
        list.add(p);
        p = new ProjectBean();
        p.setName("官方的还是");
        list.add(p);
        p = new ProjectBean();
        p.setName("给我二哥");
        list.add(p);
        p = new ProjectBean();
        p.setName("很温柔挺好");
        list.add(p);
        p = new ProjectBean();
        p.setName("瓦尔特好多个");
        list.add(p);
        return list;
    }
}
