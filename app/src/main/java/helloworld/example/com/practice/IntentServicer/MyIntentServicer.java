package helloworld.example.com.practice.IntentServicer;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class MyIntentServicer extends IntentService {

    private boolean isRun = false;
    private int count = 0;

    public MyIntentServicer() {
        super("MyIntentServicer");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            sendThreadStatus("线程正在启动-------", count);
            Thread.sleep(3000);
            sendServiceStatus("服务运行中...");

            isRun = true;
            count = 0;
            while (isRun) {
                count++;
                if (count >= 100) {
                    isRun = false;
                }
                Thread.sleep(100);
                sendThreadStatus("线程运行中...", count);
                LogUtils.e("count = "+count);
            }
            sendThreadStatus("线程结束", count);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sendServiceStatus("服务结束");
    }

    //发送服务状态信息
    private void sendServiceStatus(String status) {
        Intent intent = new Intent(MyIntentServiceActivity.ACTION_ONE);
        intent.putExtra("status", status);
        sendBroadcast(intent);
//        broadcastManager.sendBroadcast(intent);
    }

    private void sendThreadStatus(String status, int progress) {
        Intent intent = new Intent(MyIntentServiceActivity.ACTION_TWO);
        intent.putExtra("status", status);
        intent.putExtra("progress", progress);
        sendBroadcast(intent);
//        broadcastManager.sendBroadcast(intent);
    }
}
