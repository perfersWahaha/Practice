package helloworld.example.com.practice.retrofit;

import java.util.List;

/**
 * Created by Administrator on 2017/9/14 0014.
 */

public class ForestHostModel {


    /**
     * HostCode : 101000000000
     * HostName : 寄主总面积
     * LatiName : null
     * Parent :
     * FullPY : null
     * PY : null
     * HostLevel : 1
     * OtherName : null
     * Importance : null
     * OldCode : null
     * GBCode : null
     * GBName : null
     * Status : null
     * IsDel : null
     * FillFormTime : null
     * UserID : null
     * Description : null
     * EntityState : 2
     * EntityKey : {"EntitySetName":"Forest_Hosts","EntityContainerName":"ForestEntities","EntityKeyValues":[{"Key":"HostCode","Value":"101000000000"}],"IsTemporary":false}
     */

    private String HostCode;
    private String HostName;
    private Object LatiName;
    private String Parent;
    private Object FullPY;
    private Object PY;
    private String HostLevel;
    private Object OtherName;
    private Object Importance;
    private Object OldCode;
    private Object GBCode;
    private Object GBName;
    private Object Status;
    private Object IsDel;
    private Object FillFormTime;
    private Object UserID;
    private Object Description;
    private int EntityState;
    private EntityKeyBean EntityKey;

    @Override
    public String toString() {
        return "ForestHostModel{" +
                "HostCode='" + HostCode + '\'' +
                ", HostName='" + HostName + '\'' +
                ", LatiName=" + LatiName +
                ", Parent='" + Parent + '\'' +
                ", FullPY=" + FullPY +
                ", PY=" + PY +
                ", HostLevel='" + HostLevel + '\'' +
                ", OtherName=" + OtherName +
                ", Importance=" + Importance +
                ", OldCode=" + OldCode +
                ", GBCode=" + GBCode +
                ", GBName=" + GBName +
                ", Status=" + Status +
                ", IsDel=" + IsDel +
                ", FillFormTime=" + FillFormTime +
                ", UserID=" + UserID +
                ", Description=" + Description +
                ", EntityState=" + EntityState +
                ", EntityKey=" + EntityKey +
                '}';
    }

    public String getHostCode() {
        return HostCode;
    }

    public void setHostCode(String HostCode) {
        this.HostCode = HostCode;
    }

    public String getHostName() {
        return HostName;
    }

    public void setHostName(String HostName) {
        this.HostName = HostName;
    }

    public Object getLatiName() {
        return LatiName;
    }

    public void setLatiName(Object LatiName) {
        this.LatiName = LatiName;
    }

    public String getParent() {
        return Parent;
    }

    public void setParent(String Parent) {
        this.Parent = Parent;
    }

    public Object getFullPY() {
        return FullPY;
    }

    public void setFullPY(Object FullPY) {
        this.FullPY = FullPY;
    }

    public Object getPY() {
        return PY;
    }

    public void setPY(Object PY) {
        this.PY = PY;
    }

    public String getHostLevel() {
        return HostLevel;
    }

    public void setHostLevel(String HostLevel) {
        this.HostLevel = HostLevel;
    }

    public Object getOtherName() {
        return OtherName;
    }

    public void setOtherName(Object OtherName) {
        this.OtherName = OtherName;
    }

    public Object getImportance() {
        return Importance;
    }

    public void setImportance(Object Importance) {
        this.Importance = Importance;
    }

    public Object getOldCode() {
        return OldCode;
    }

    public void setOldCode(Object OldCode) {
        this.OldCode = OldCode;
    }

    public Object getGBCode() {
        return GBCode;
    }

    public void setGBCode(Object GBCode) {
        this.GBCode = GBCode;
    }

    public Object getGBName() {
        return GBName;
    }

    public void setGBName(Object GBName) {
        this.GBName = GBName;
    }

    public Object getStatus() {
        return Status;
    }

    public void setStatus(Object Status) {
        this.Status = Status;
    }

    public Object getIsDel() {
        return IsDel;
    }

    public void setIsDel(Object IsDel) {
        this.IsDel = IsDel;
    }

    public Object getFillFormTime() {
        return FillFormTime;
    }

    public void setFillFormTime(Object FillFormTime) {
        this.FillFormTime = FillFormTime;
    }

    public Object getUserID() {
        return UserID;
    }

    public void setUserID(Object UserID) {
        this.UserID = UserID;
    }

    public Object getDescription() {
        return Description;
    }

    public void setDescription(Object Description) {
        this.Description = Description;
    }

    public int getEntityState() {
        return EntityState;
    }

    public void setEntityState(int EntityState) {
        this.EntityState = EntityState;
    }

    public EntityKeyBean getEntityKey() {
        return EntityKey;
    }

    public void setEntityKey(EntityKeyBean EntityKey) {
        this.EntityKey = EntityKey;
    }

    public static class EntityKeyBean {
        /**
         * EntitySetName : Forest_Hosts
         * EntityContainerName : ForestEntities
         * EntityKeyValues : [{"Key":"HostCode","Value":"101000000000"}]
         * IsTemporary : false
         */

        private String EntitySetName;
        private String EntityContainerName;
        private boolean IsTemporary;
        private List<EntityKeyValuesBean> EntityKeyValues;

        @Override
        public String toString() {
            return "EntityKeyBean{" +
                    "EntitySetName='" + EntitySetName + '\'' +
                    ", EntityContainerName='" + EntityContainerName + '\'' +
                    ", IsTemporary=" + IsTemporary +
                    ", EntityKeyValues=" + EntityKeyValues +
                    '}';
        }

        public String getEntitySetName() {
            return EntitySetName;
        }

        public void setEntitySetName(String EntitySetName) {
            this.EntitySetName = EntitySetName;
        }

        public String getEntityContainerName() {
            return EntityContainerName;
        }

        public void setEntityContainerName(String EntityContainerName) {
            this.EntityContainerName = EntityContainerName;
        }

        public boolean isIsTemporary() {
            return IsTemporary;
        }

        public void setIsTemporary(boolean IsTemporary) {
            this.IsTemporary = IsTemporary;
        }

        public List<EntityKeyValuesBean> getEntityKeyValues() {
            return EntityKeyValues;
        }

        public void setEntityKeyValues(List<EntityKeyValuesBean> EntityKeyValues) {
            this.EntityKeyValues = EntityKeyValues;
        }

        public static class EntityKeyValuesBean {
            /**
             * Key : HostCode
             * Value : 101000000000
             */

            private String Key;
            private String Value;

            @Override
            public String toString() {
                return "EntityKeyValuesBean{" +
                        "Key='" + Key + '\'' +
                        ", Value='" + Value + '\'' +
                        '}';
            }

            public String getKey() {
                return Key;
            }

            public void setKey(String Key) {
                this.Key = Key;
            }

            public String getValue() {
                return Value;
            }

            public void setValue(String Value) {
                this.Value = Value;
            }
        }
    }
}
