package helloworld.example.com.practice.mvp;

/**
 * Created by Administrator on 2017/8/30 0030.
 */

public interface ILoginView {
    String getUserName();
    String getPassWord();
    void dimessProgress();
    void showProgress();
    void showView();
    void showFailedError();
}
