package helloworld.example.com.practice.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2017/9/14 0014.
 */

public class ApiHelper {

    private static Gson mGson;

    private static void init(){
        mGson= new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .serializeNulls()
                .create();
    }



    static Retrofit retrofit(String baseUrl) {
        if (mGson==null){
            init();
        }
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .build();
    }
}
