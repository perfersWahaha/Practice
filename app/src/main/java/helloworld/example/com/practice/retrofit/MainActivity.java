package helloworld.example.com.practice.retrofit;

import android.app.Activity;
import android.os.Bundle;
import android.renderscript.Type;
import android.support.annotation.Nullable;

import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/13 0013.
 */

public class MainActivity extends Activity {

    private List<ForestHostModel> mData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         Retrofit retrofit = new Retrofit.Builder()
         .baseUrl("https://api.github.com/")
         .addConverterFactory(GsonConverterFactory.create())
         .build();

         Api service = retrofit.create(Api.class);
         Call<TestModel> model = service.repo("Guolei1130");
         model.enqueue(new Callback<TestModel>() {
        @Override public void onResponse(Call<TestModel> call, Response<TestModel> response) {
        LogUtils.e(response.body().toString());
        }

        @Override public void onFailure(Call<TestModel> call, Throwable t) {
        LogUtils.e(t.getMessage());
        }
        });



         Gson mGson = new GsonBuilder()
         .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
         .serializeNulls()
         .create();

         ServiceResponse responce = new ServiceResponse();
         responce.setTaskID("");
         responce.setTrapperCode("海南苏铁");
         String responceJson = mGson.toJson(responce);

         RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),responceJson);

         Retrofit retrofit1 = new Retrofit.Builder()
         .baseUrl("http://forest.cqsfz.cn/PhoneService/Common/")
         .addConverterFactory(GsonConverterFactory.create(mGson))
         .build();

         LogUtils.e("requestBody = "+requestBody.toString());
         Api service1 = retrofit1.create(Api.class);
         Call<List<ForestHostModel>> forestHost = service1.getDataBaseData("forest_host",responceJson);
         forestHost.enqueue(new Callback<List<ForestHostModel>>() {
        @Override public void onResponse(Call<List<ForestHostModel>> call, Response<List<ForestHostModel>> response) {
        LogUtils.e(response.body().toString());
        mData.addAll(response.body());
        LogUtils.e(mData+"");
        }

        @Override public void onFailure(Call<List<ForestHostModel>> call, Throwable t) {
        LogUtils.e(t.getMessage());
        }

        });
         */

//        ApiController.getDataBaseData("forest_host", "", "海南苏铁", new RetrofitListener<List<?>>() {
//            @Override
//            public void onSuccess(List<?> data) {
//                LogUtils.e("data = "+data);
//            }
//
//            @Override
//            public void onError(String description) {
//
//            }
//        });

//        ApiController.getTData("forest_host", "", "海南苏铁", ForestHostModel.class, new RetrofitListener<List<?>>() {
//            @Override
//            public void onSuccess(List<?> data) {
//                LogUtils.e("data = " + data);
//            }
//
//            @Override
//            public void onError(String description) {
//
//            }
//        });
        MyRequestParameters request = new MyRequestParameters();
        request.put("TrapperCode","苏铁");
        request.put("TaskID","700");
        LogUtils.e("request = "+request.buildRequestString());
        LogUtils.e("request = "+request.buildRequestJson());

        ApiController.getData("forest_host",request.buildRequestString(), ForestHostModel.class, new RetrofitListener<List<?>>() {
            @Override
            public void onSuccess(List<?> data) {
                LogUtils.e("data = " + data);
            }

            @Override
            public void onError(String description) {

            }
        });
    }
}
