package helloworld.example.com.practice.game;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 2017/11/8.
 */

public class MyGameView extends GridLayout {

    // 6 定义一个二维数组来记录这些卡片
    private MyCard[][] cards = new MyCard[4][4];
    //7 定义一个集合来保存卡片的位置
    private List<Point> pointList = new ArrayList<>();

    public MyGameView(Context context) {
        super(context);
        //初始化游戏视图
        initGameView();
    }

    public MyGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //初始化游戏视图
        initGameView();
    }

    public MyGameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //初始化游戏视图
        initGameView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyGameView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        //初始化游戏视图
        initGameView();
    }

    //初始化游戏视图
    private void initGameView() {
        //设置显示4列
        setColumnCount(4);
        //设置背景颜色
        setBackgroundColor(0xffbbada0);
        //屏幕触摸事件监听
        setOnTouchListener(new OnTouchListener() {

            private float downX;
            private float upX;
            private float downY;
            private float upY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //记录点下时的x、y坐标
                        downX = event.getX();
                        downY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        //记录抬起时的x、y坐标
                        upX = event.getX();
                        upY = event.getY();
                        //判断滑动的方向(Math.abs()得到绝对值)
                        if (Math.abs(upX - downX) > Math.abs(upY - downY)) {
                            //如果移动的x>移动的y，即滑动的方向为水平方向
                            if (upX - downX > 5) {
                                //如果抬起时的x大于点下时的x，即水平向右滑动
                                swipeRight();
                            } else {
                                //否则水平向左滑动
                                swipeLeft();
                            }
                            addRandomNum();
                        } else {
                            //否则为垂直方向
                            //如果移动的x>移动的y，即滑动的方向为水平方向
                            if (upY - downY < 5) {
                                //如果抬起时的y小于点下时的y，即垂直向上滑动
                                swipeUp();
                            } else {
                                //否则垂直向上滑动
                                swipeDown();
                            }
                            addRandomNum();
                        }
                        break;
                }
                return true;
            }
        });
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //5.1求出卡片的宽高
        //用最小值的用意：布局是正方形 手机的屏幕是长方形 宽度大 还是高度大我以 最小值为准
        //-10像素 图形到屏幕边缘留下一个空隙
        // /4 得到每一张卡片的高度
        int cardWidth = (Math.min(w, h) - 10) / 4;
        //初始化16个卡片对象
        addCards(cardWidth, cardWidth);

        //开始游戏
        startGame();

    }

    //给每个位置添加一张卡片
    private void addCards(int cardWidth, int cardHeight) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                MyCard card = new MyCard(getContext());
                card.setNum(0);
                addView(card, cardWidth, cardHeight);
                cards[j][i] = card;
            }
        }
    }

    //向左滑动
    private void swipeLeft() {
        //遍历所有卡片
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                //这个for循环是遍历当前卡片之后的当前行的卡片
                for (int x = j + 1; x < 4; x++) {
                    if (cards[x][i].getNum() != 0) {
                        //判断当前的卡片是否为0
                        if (cards[j][i].getNum() == 0) {
                            //如果当前卡片为0则把该卡片后的值替换第一个有值的卡片的值
                            cards[j][i].setNum(cards[x][i].getNum());
                            //后一张卡片值给了当前卡片，所以值应该被设置为0
                            cards[x][i].setNum(0);
                            //当前卡片值被改变后，应该重新遍历判断当前卡片后面的卡片，所以j--;
                            j--;
                        } else if (cards[x][i].equals(cards[j][i])) {
                            //如果当前卡片的值等于后面第一张有值卡片的值，合并值
                            cards[j][i].setNum(cards[j][i].getNum() * 2);
                            //后一张卡片值给了当前卡片，所以值应该被设置为0
                            cards[x][i].setNum(0);
                        }
                        //结束循环
                        break;
                    }
                }
            }
        }
    }

    //向右滑动(自己理解，逻辑同向左滑动)
    private void swipeRight() {
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >= 0; j--) {
                for (int x = j - 1; x >= 0; x--) {
                    if (cards[x][i].getNum() != 0) {
                        if (cards[j][i].getNum() == 0) {
                            cards[j][i].setNum(cards[x][i].getNum());
                            cards[x][i].setNum(0);
                            j++;
                        } else if (cards[x][i].equals(cards[j][i])) {
                            cards[j][i].setNum(cards[j][i].getNum() * 2);
                            cards[x][i].setNum(0);
                        }
                        break;
                    }
                }
            }
        }
    }

    //向上滑动(自己理解，逻辑同向左滑动)
    private void swipeUp() {
        //遍历所有卡片
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                //这个for循环是遍历当前卡片之后的当前列的卡片
                for (int y = i + 1; y < 4; y++) {
                    if (cards[j][y].getNum() != 0) {
                        //判断当前的卡片是否为0
                        if (cards[j][i].getNum() == 0) {
                            //如果当前卡片为0则把该卡片后的值替换第一个有值的卡片的值
                            cards[j][i].setNum(cards[j][y].getNum());
                            //后一张卡片值给了当前卡片，所以值应该被设置为0
                            cards[j][y].setNum(0);
                            //当前卡片值被改变后，应该重新遍历判断当前卡片后面的卡片，所以j--;
                            i--;
                        } else if (cards[j][y].equals(cards[j][i])) {
                            //如果当前卡片的值等于后面第一张有值卡片的值，合并值
                            cards[j][i].setNum(cards[j][i].getNum() * 2);
                            //后一张卡片值给了当前卡片，所以值应该被设置为0
                            cards[j][y].setNum(0);
                        }
                        //结束循环
                        break;
                    }
                }
            }
        }
    }

    //向下滑动(自己理解，逻辑同向左滑动)
    private void swipeDown() {
        for (int j = 0; j < 4; j++) {
            for (int i = 3; i >= 0; i--) {
                for (int y = i - 1; y >= 0; y--) {
                    if (cards[j][y].getNum() != 0) {
                        if (cards[j][i].getNum() == 0) {
                            cards[j][i].setNum(cards[j][y].getNum());
                            cards[j][y].setNum(0);
                            i++;
                        } else if (cards[j][y].equals(cards[j][i])) {
                            cards[j][i].setNum(cards[j][i].getNum() * 2);
                            cards[j][y].setNum(0);
                        }
                        break;
                    }
                }
            }
        }
    }

    //开始游戏
    private void startGame() {
        //所有的卡片回到初始状态(即数字都为0)
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                cards[j][i].setNum(0);
            }
        }
        //产生一个随机的2或者4，出现在随机位置(默认开始的时候，有两个)
        addRandomNum();
        addRandomNum();
    }


    //添加随机数 2 或者 4
    private void addRandomNum() {
        //清除原来保存的位置
        pointList.clear();
        //遍历二维数组
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                //判断卡片对象的num是否为0
                if (cards[j][i].getNum() <= 0) {
                    //如果num==0，则没有数字，那么该卡片为可以产生新数字的卡片，记录下该卡片的位置
                    pointList.add(new Point(j, i));
                }
            }
        }
        if (pointList.size() > 0) {
            //得到产生新num的卡片位置
            Point point = pointList.remove((int) (Math.random() * pointList.size()));
            //随机产生2或者4，并设置到卡片上，Math.random() > 0.1 ? 2 : 4 概率 90%出现2
            cards[point.x][point.y].setNum(Math.random() > 0.1 ? 2 : 4);
        } else {
            if (hasSameCard()){
                return;
            }else {
                Toast.makeText(getContext(), "游戏结束", Toast.LENGTH_SHORT).show();
            }
        }

    }

    //判断是否还有相同的卡片
    private boolean hasSameCard() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j<4;j++){
                if (j<3) {
                    //判断每行相邻的数据是否相同
                    if (cards[j][i].equals(cards[j+1][i])){
                        return true;
                    }
                    //判断每列相邻的数据是否相同
                    if (cards[i][j].equals(cards[i][j+1])){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
