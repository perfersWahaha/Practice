package helloworld.example.com.practice.recyclerview;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import helloworld.example.com.practice.R;

public class MainActivity extends AppCompatActivity {

    //声明控件
    private RecyclerView recyclerView;
    private Button btnEditUser;

    private List<User> userList = new ArrayList<>();

    private UserRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_huadong);
        //获取控件
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        btnEditUser = (Button) findViewById(R.id.btn_edit_user);

        //初始化recyclervie
        initRecyclerView();
        //获取、更新数据
        getDataToRecyclerView();

        //设置button的点击事件
        btnEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setShowDelete(!adapter.isShowDelete());
                adapter.notifyDataSetChanged();
            }
        });

    }

    //初始化recyclerView
    private void initRecyclerView(){
        //布局管理（垂直）
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //分割线
        RecycleViewDivider itemDecoration = new RecycleViewDivider(this, DividerItemDecoration.VERTICAL, 1, Color.parseColor("#dddddd"));
        //删除动画
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        //适配器
        adapter = new UserRvAdapter(this,userList);
        //设置不显示删除
        adapter.setShowDelete(false);
        //设置删除按钮的点击事件(没有实现该方法会报错，你可以注释掉该方法试试，会给你提示的)
        adapter.setOnDeleteClickListener(new UserRvAdapter.OnDeleteClickListener() {
            @Override
            public void onDeleteClick(View v, int position, User user) {
                //删除点击的数据
                userList.remove(position);
                adapter.notifyItemRemoved(position);


                Toast.makeText(MainActivity.this,"点击了删除按钮，删除了"+user.getName(),Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.setAdapter(adapter);
    }

    /**
     * 得到数据,并更新到recyclerView上
     */
    private void getDataToRecyclerView(){

        //模拟数据
        userList.clear();
        for (int i=0;i<15;i++){
            User user = new User();
            user.setName("xiaoA"+i);
            user.setAge(18+i);
            user.setSex(i%2==0?"男":"女");
            userList.add(user);
        }
        //更新到recyclerView上
        if (adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 实体类
     */
    class User{
        private String name;
        private int age;
        private String sex;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }
    }

}
