package helloworld.example.com.practice.retrofit;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Administrator on 2017/9/13 0013.
 */

public interface Api {

    @GET("users/{user}")
    Call<TestModel> repo(@Path("user") String user);

    @POST("GetData.ashx")
    Call<List<ForestHostModel>> getDataBaseData(@Query("type") String type,@Query("where")String json);

    @POST("GetData.ashx")
    Call<ResponseBody> getTData(@Query("type")String type,@Query("where")String json);

    @POST("GetData.ashx")
    Call<ForestHostModel> getListData(@Query("type")String type,@Query("where")String json);
}
