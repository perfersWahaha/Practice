package helloworld.example.com.practice.duoxiancheng;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import helloworld.example.com.practice.utils.LogUtils;

/**
 * Created by Administrator on 2017/9/26 0026.
 */

public class ThreadTestActivity extends Activity{

    private ThreadPoolExecutor executor;
    private ExecutorService executors;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        executor = new ThreadPoolExecutor(9,200,60, TimeUnit.SECONDS,new LinkedBlockingDeque<Runnable>());
        executors = Executors.newFixedThreadPool(9);
    }

    private class MyRunnable implements Runnable{
        @Override
        public void run() {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i <20 ; i++) {
                LogUtils.e("MyRunnable i的值为: "+i);
                s.append(i);
            }
            LogUtils.e(s.toString());
        }
    }

    private class MyThread extends Thread{
        @Override
        public void run() {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i <20 ; i++) {
                LogUtils.e("MyThread i的值为: "+i);
                s.append(i);
            }
            LogUtils.e(s.toString());
        }
    }

    private class MyAsyncTask extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i <20 ; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LogUtils.e("MyAsyncTask i的值为: "+i);
            }
            return null;
        }
    }


}
