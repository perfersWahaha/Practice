package helloworld.example.com.practice.recycleview;

/**
 * Created by Administrator on 2017/10/20 0020.
 */

public class ProjectBean {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "ProjectBean{" +
                "name='" + name + '\'' +
                '}';
    }
}
